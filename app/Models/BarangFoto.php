<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class BarangFoto
 *
 * @property int $id
 * @property string $foto_url
 * @property int $utama
 * @property int $barang_id
 *
 * @property \App\Models\Barang $barang
 *
 * @package App\Models
 */
class BarangFoto extends Eloquent
{
	protected $table = 'barang_foto';
	public $timestamps = false;

	protected $casts = [
		'utama' => 'int',
		'barang_id' => 'int'
	];

	protected $fillable = [
		'foto_url',
		'utama',
		'barang_id'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function getFotoUrlAttribute($value)
  {
		if (filter_var($value, FILTER_VALIDATE_URL)) {
			return $value;
		}
		return Storage::disk('public')->url($value);
  }
}
