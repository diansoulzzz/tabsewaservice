<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HSewa
 * 
 * @property int $id
 * @property string $kodenota
 * @property \Carbon\Carbon $tgl
 * @property \Carbon\Carbon $tgl_sewa_awal
 * @property \Carbon\Carbon $tgl_sewa_akhir
 * @property int $lama_sewa
 * @property float $harga_sewa
 * @property float $deposit
 * @property float $grandtotal
 * @property float $sisabayar
 * @property float $terbayar
 * @property int $status_approve
 * @property \Carbon\Carbon $tgl_konfirmasi
 * @property \Carbon\Carbon $tgl_dikirim
 * @property \Carbon\Carbon $tgl_diterima
 * @property \Carbon\Carbon $tgl_batas_kembali
 * @property \Carbon\Carbon $tgl_kembali
 * @property float $deposit_kembali
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $barang_id
 * @property int $users_penyewa_id
 * 
 * @property \App\Models\Barang $barang
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $h_piutangs
 * @property \Illuminate\Database\Eloquent\Collection $review_barangs
 *
 * @package App\Models
 */
class HSewa extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_sewa';

	protected $casts = [
		'lama_sewa' => 'int',
		'harga_sewa' => 'float',
		'deposit' => 'float',
		'grandtotal' => 'float',
		'sisabayar' => 'float',
		'terbayar' => 'float',
		'status_approve' => 'int',
		'deposit_kembali' => 'float',
		'barang_id' => 'int',
		'users_penyewa_id' => 'int'
	];

	protected $dates = [
		'tgl',
		'tgl_sewa_awal',
		'tgl_sewa_akhir',
		'tgl_konfirmasi',
		'tgl_dikirim',
		'tgl_diterima',
		'tgl_batas_kembali',
		'tgl_kembali'
	];

	protected $fillable = [
		'kodenota',
		'tgl',
		'tgl_sewa_awal',
		'tgl_sewa_akhir',
		'lama_sewa',
		'harga_sewa',
		'deposit',
		'grandtotal',
		'sisabayar',
		'terbayar',
		'status_approve',
		'tgl_konfirmasi',
		'tgl_dikirim',
		'tgl_diterima',
		'tgl_batas_kembali',
		'tgl_kembali',
		'deposit_kembali',
		'barang_id',
		'users_penyewa_id'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_penyewa_id');
	}

	public function h_piutangs()
	{
		return $this->hasMany(\App\Models\HPiutang::class);
	}

	public function review_barangs()
	{
		return $this->hasMany(\App\Models\ReviewBarang::class);
	}
}
