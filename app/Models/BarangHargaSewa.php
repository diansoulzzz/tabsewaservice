<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangHargaSewa
 * 
 * @property int $id
 * @property int $barang_id
 * @property int $lama_hari
 * @property float $harga
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Barang $barang
 *
 * @package App\Models
 */
class BarangHargaSewa extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang_harga_sewa';

	protected $casts = [
		'barang_id' => 'int',
		'lama_hari' => 'int',
		'harga' => 'float'
	];

	protected $fillable = [
		'barang_id',
		'lama_hari',
		'harga'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}
}
