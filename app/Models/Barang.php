<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Barang
 *
 * @property int $id
 * @property string $nama
 * @property int $barang_sub_kategori_id
 * @property int $users_id
 * @property int $stok
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property float $deposit_min
 *
 * @property \App\Models\BarangSubKategori $barang_sub_kategori
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $barang_fotos
 * @property \Illuminate\Database\Eloquent\Collection $barang_harga_sewas
 * @property \Illuminate\Database\Eloquent\Collection $h_sewas
 *
 * @package App\Models
 */
class Barang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang';

	protected $casts = [
		'barang_sub_kategori_id' => 'int',
		'users_id' => 'int',
		'stok' => 'int',
		'deposit_min' => 'float'
	];

	protected $fillable = [
		'nama',
		'barang_sub_kategori_id',
		'users_id',
		'stok',
		'deposit_min'
	];

	public function barang_sub_kategori()
	{
		return $this->belongsTo(\App\Models\BarangSubKategori::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function barang_fotos()
	{
		return $this->hasMany(\App\Models\BarangFoto::class);
	}

	public function barang_harga_sewas()
	{
		return $this->hasMany(\App\Models\BarangHargaSewa::class);
	}

	public function h_sewas()
	{
		return $this->hasMany(\App\Models\HSewa::class);
	}
	//CUSTOM
	public function barang_foto_utama()
	{
		return $this->hasOne(\App\Models\BarangFoto::class)->where('utama',1);
	}
}
