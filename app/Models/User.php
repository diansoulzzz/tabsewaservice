<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:23 +0000.
 */

 namespace App\Models;

 use Illuminate\Notifications\Notifiable;
 use Laravel\Passport\HasApiTokens;
 use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $foto_url
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $aktif
 * @property string $provider
 * @property string $provider_id
 *
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 * @property \Illuminate\Database\Eloquent\Collection $barang_kategoris
 * @property \Illuminate\Database\Eloquent\Collection $barang_sub_kategoris
 * @property \Illuminate\Database\Eloquent\Collection $h_piutangs
 * @property \Illuminate\Database\Eloquent\Collection $h_sewas
 * @property \Illuminate\Database\Eloquent\Collection $review_barangs
 *
 * @package App\Models
 */
 class User extends Authenticatable
 {
 	use HasApiTokens, Notifiable;
	protected $casts = [
		'aktif' => 'int'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'foto_url',
		'remember_token',
		'aktif',
		'provider',
		'provider_id'
	];

	public function barangs()
	{
		return $this->hasMany(\App\Models\Barang::class, 'users_id');
	}

	public function barang_kategoris()
	{
		return $this->hasMany(\App\Models\BarangKategori::class, 'users_id');
	}

	public function barang_sub_kategoris()
	{
		return $this->hasMany(\App\Models\BarangSubKategori::class, 'users_id');
	}

	public function h_piutangs()
	{
		return $this->hasMany(\App\Models\HPiutang::class, 'users_peminjam_id');
	}

	public function h_sewas()
	{
		return $this->hasMany(\App\Models\HSewa::class, 'users_penyewa_id');
	}

	public function review_barangs()
	{
		return $this->hasMany(\App\Models\ReviewBarang::class, 'users_id');
	}
}
