<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DSewa
 * 
 * @property int $id
 * @property int $lama_sewa
 * @property float $harga_sewa
 * @property float $subtotal
 * @property \Carbon\Carbon $batas_pengembalian
 * @property \Carbon\Carbon $tgl_kembali
 * @property int $approve_sewa
 *
 * @package App\Models
 */
class DSewa extends Eloquent
{
	protected $table = 'd_sewa';
	public $timestamps = false;

	protected $casts = [
		'lama_sewa' => 'int',
		'harga_sewa' => 'float',
		'subtotal' => 'float',
		'approve_sewa' => 'int'
	];

	protected $dates = [
		'batas_pengembalian',
		'tgl_kembali'
	];

	protected $fillable = [
		'lama_sewa',
		'harga_sewa',
		'subtotal',
		'batas_pengembalian',
		'tgl_kembali',
		'approve_sewa'
	];
}
