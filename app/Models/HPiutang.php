<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HPiutang
 * 
 * @property int $id
 * @property string $kodenota
 * @property \Carbon\Carbon $tgl
 * @property float $total_bayar
 * @property string $payment_method
 * @property string $status_pembayaran
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_peminjam_id
 * @property int $h_sewa_id
 * 
 * @property \App\Models\HSewa $h_sewa
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class HPiutang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_piutang';

	protected $casts = [
		'total_bayar' => 'float',
		'users_peminjam_id' => 'int',
		'h_sewa_id' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'kodenota',
		'tgl',
		'total_bayar',
		'payment_method',
		'status_pembayaran',
		'users_peminjam_id',
		'h_sewa_id'
	];

	public function h_sewa()
	{
		return $this->belongsTo(\App\Models\HSewa::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_peminjam_id');
	}
}
