<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:23 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ReviewBarang
 * 
 * @property int $id
 * @property int $rating
 * @property string $komentar
 * @property string $foto_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $h_sewa_id
 * @property int $users_id
 * 
 * @property \App\Models\HSewa $h_sewa
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class ReviewBarang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'review_barang';

	protected $casts = [
		'rating' => 'int',
		'h_sewa_id' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'rating',
		'komentar',
		'foto_url',
		'h_sewa_id',
		'users_id'
	];

	public function h_sewa()
	{
		return $this->belongsTo(\App\Models\HSewa::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
