<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 20 Mar 2018 06:40:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cart
 * 
 * @property int $id
 * @property \Carbon\Carbon $tgl_awal
 * @property \Carbon\Carbon $tgl_akhir
 * @property int $lama_sewa
 *
 * @package App\Models
 */
class Cart extends Eloquent
{
	protected $table = 'cart';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'lama_sewa' => 'int'
	];

	protected $dates = [
		'tgl_awal',
		'tgl_akhir'
	];

	protected $fillable = [
		'tgl_awal',
		'tgl_akhir',
		'lama_sewa'
	];
}
