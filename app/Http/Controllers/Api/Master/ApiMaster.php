<?php
namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangFoto;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;

class ApiMaster extends Controller
{
  public function postData(Request $request)
  {
    $barang = new Barang();
    $barang->nama=$request->input('nama');
    $barang->barang_sub_kategori_id=$request->input('barang_sub_kategori_id');
    $barang->users_id=$request->input('users_id');
    $barang->stok=$request->input('stok');
    $barang->deposit_min=$request->input('deposit_min');
    $barang->save();
    foreach ($request->input('foto') as $key => $foto) {
      $barangfoto = new BarangFoto();
      $barangfoto->foto_url = $foto->foto_url;
      $barangfoto->utama = $foto->utama;
      $barangfoto->barang_id = $foto->barang_id;
      $barangfoto->save();
    }
    // $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
  }
  public function getData(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    $promo = Barang::get();
    $kategori = BarangKategori::get();
    $subkategori = BarangSubKategori::get();
    $data= [
      'barang' => $barang,
      'promo' => $promo,
      'kategori' => $kategori,
      'subkategori' => $subkategori,
    ];
    return response()->json(['data' => $data], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemLatest(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemList(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemDetail(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->find($request->input('id'));
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
}
