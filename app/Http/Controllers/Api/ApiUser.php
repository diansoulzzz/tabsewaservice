<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ApiUser extends Controller
{

  public function index(Request $request)
  {
    // return $request->user();
    $user = User::find(1);
    $message = 'ok';
    $status = '1';
    return response()->json(['data' => $user,'message'=> $message,'status'=> $status]);
    // return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getUserInfo(Request $request)
  {
      $user = $request->user();
      $message = 'ok';
      $status = '1';
      return response()->json(['data' => $user,'message'=> $message,'status'=> $status]);
  }

  public function GetUser(Request $request)
  {
    return Posting::with('user')->get(); //select * from posting
  }

  public function CreateUser(Request $request)
  {
    $user = new User(); //Insert
    $user->name = 'toni';
    $user->password = bcrypt('123456');
    $user->email = 'toni@gmail.com';
    $user->save();
  }

  public function StoreData(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
      return $result;
    }
    $user = new User();
    $user->name = $request->input('name');
    $user->password = bcrypt($request->input('password'));
    $user->email = $request->input('email');
    $user->save();
    $status = ['code'=>'1','message'=>'OK'];
    $result = [
      'status' => $status,
      'request' => $request->all(),
    ];
    return $result;
  }

  public function GetData(Request $request)
  {
    $data = User::get();
    if ($data->isEmpty())
    {
      $status = ['code'=>'0','message'=>'error'];
      $result = [
        'status' => $status,
        'request' => $request->all(),
      ];
    }
    else {
      $status = ['code'=>'1','message'=>'OK'];
      $result = [
        'status' => $status,
        'result' => $data,
        'request' => $request->all(),
      ];
    }
    return response()->json($result);
  }
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'email' => 'required',
          'password' => 'required',
      ]);
  }
  public function Logout()
  {
    Auth::logout();
    return redirect(route('login'));
  }
}
