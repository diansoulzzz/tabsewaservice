<?php
namespace App\Http\Controllers\Api\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\BarangSubKategori;

class ApiHome extends Controller
{
  public function index(Request $request)
  {

  }
  public function getData(Request $request)
  {
    // $barang = Barang::with(['barang_sub_kategori.barang_kategori',
    //   'barang_fotos' => function ($query){
    //     return $query->where('utama',1)->first();
    // }])->get();
    // foreach ($barang as $key => $value) {
    //   if ($barang[$key]->barang_fotos->count()>0)
    //   {
    //       $barang[$key]->foto_url = $barang[$key]->barang_fotos->first()->foto_url;
    //   }
    //   else {
    //       $barang[$key]->foto_url = 'https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAeLAAAAJDgzNDBlMmI4LTE2NGUtNDU3MS05MmNjLTJlYmQzNmNhYWNiNA.png';
    //   }
    // }
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    $promo = Barang::get();
    $kategori = BarangKategori::get();
    $subkategori = BarangSubKategori::get();
    $data= [
      'barang' => $barang,
      'promo' => $promo,
      'kategori' => $kategori,
      'subkategori' => $subkategori,
    ];
    return response()->json(['data' => $data], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemLatest(Request $request)
  {
    // $barang = Barang::with(['barang_sub_kategori.barang_kategori',
    //   'barang_fotos' => function ($query){
    //     return $query->where('utama',1)->first();
    // }])->get();
    // foreach ($barang as $key => $value) {
    //   if ($barang[$key]->barang_fotos->count()>0)
    //   {
    //       $barang[$key]->foto_url = $barang[$key]->barang_fotos->first()->foto_url;
    //   }
    //   else {
    //       $barang[$key]->foto_url = 'https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAeLAAAAJDgzNDBlMmI4LTE2NGUtNDU3MS05MmNjLTJlYmQzNmNhYWNiNA.png';
    //   }
    // }
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemList(Request $request)
  {
    // $barang = Barang::with(['barang_sub_kategori.barang_kategori',
    //   'barang_fotos' => function ($query){
    //     return $query->where('utama',1)->first();
    // }])->get();
    // foreach ($barang as $key => $value) {
    //   if ($barang[$key]->barang_fotos->count()>0)
    //   {
    //       $barang[$key]->foto_url = $barang[$key]->barang_fotos->first()->foto_url;
    //   }
    //   else {
    //       $barang[$key]->foto_url = 'https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAeLAAAAJDgzNDBlMmI4LTE2NGUtNDU3MS05MmNjLTJlYmQzNmNhYWNiNA.png';
    //   }
    // }
    // $barang->url_photo = $barang->barang_fotos->first()->foto_url;
    // return $barang;
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->get();
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
  public function getItemDetail(Request $request)
  {
    $barang = Barang::with(['user','barang_foto_utama','barang_fotos','barang_sub_kategori.barang_kategori'])->find($request->input('id'));
    return response()->json(['data' => $barang], 200, [], JSON_NUMERIC_CHECK);
  }
}
