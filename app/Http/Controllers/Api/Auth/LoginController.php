<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Socialite;
// use App\Http\Controllers\Api\ApiUser;

class LoginController extends Controller
{
    use IssueTokenTrait;

    public function loginSocialFacebook(Request $request)
    {
      // return $request->all();
      // $user = User::create([
      //     'name' => 'sa',
      //     'email' => 'sa@tabsewa.com',
      //     'password' => Hash::make(123456),
      //     'gender' => 'Male',
      //     'biodata' => '',
      //     'alamat' => '',
      // ]);
      // return;
      // $request->merge(['email'=>'sa@tabsewa.com','password'=>'123456']);
      // return $this->issueToken($request, 'password');
      // $token = 'EAACdZCsIUu3gBACwbbpaqnZALwJIKcca27Pqg13p6K0gsuHuy2rdKrLYZBuXP7imktLexkmG3HKExRd1tr6zcDZAHINKGpZAZBBMZClozpOO9fMO0NUZCPKpHqmOlZCB4BcbmNZAtnbZAM1FREvPhBMOr7xObwG75O2hzMguUjxybBp349HzMcBxbWSPcdXvVYzIHKy5rLPo99JJDZCBARpcdlwobXClDIQsvTNAM4fzZBQpme0pZBDY1P6ZBBG';
      $token = $request->input('access_token');
      $social = Socialite::driver('facebook')->userFromToken($token);
      $user = User::where('provider_id',$social->id)->first();
      if (!$user)
      {
        $user = new User();
        $user->name = $social->name;
        $user->email = $social->email;
        $user->provider_id = $social->id;
        $user->provider = 'facebook';
        $user->aktif = '1';
        $user->save();
      }
      $token = $user->createToken('social')->accessToken;
      return $this->getLoginInfo($request,$token);
      // $request->merge($user->toArray());
      // return $this->issueToken($request, 'authorization_code', $token);
      // $message = 'ok';
      // $status = '1';
      // return $token;
      // return response()->json(['data' => $user,'message'=> $message,'status'=> $status]);
      //EXAMPLE
    }
    public function loginSocialGoogle(Request $request)
    {
      $token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6ImJhNGRlZDdmNWE5MjQyOWYyMzM1NjFhMzZmZjYxM2VkMzg3NjJjM2QifQ.eyJhenAiOiI3NDAzNTQ0MjMzMzMtMG5ydWdtZXAzbzZrMGk2bnRmMjFpbjR2YnBhb2E4dm0uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3NDAzNTQ0MjMzMzMtdDMyaWludDJ1NDRzOHBlbjE4NWJsb2M5MXZna3VyOGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDgzODY2OTIxNDMyMjUyMjU2NjEiLCJlbWFpbCI6ImRpYW4ueXVsaXVzQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJleHAiOjE1MTc4MTc4NDksImlzcyI6Imh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbSIsImlhdCI6MTUxNzgxNDI0OSwibmFtZSI6IkRpYW4gTnVncmFoYSIsInBpY3R1cmUiOiJodHRwczovL2xoNi5nb29nbGV1c2VyY29udGVudC5jb20vLXZGcHAzZGQ0LUhVL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FDU0lMalZaT2JYRk5ZcGxGWjE3dGV0SEtVSzV3bFZjc1Evczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IkRpYW4iLCJmYW1pbHlfbmFtZSI6Ik51Z3JhaGEiLCJsb2NhbGUiOiJlbi1NWSJ9.X6S7Y4C8IZUsHR_wRR4-5trQlDWKzKMwTJiVWfqIbLBj-dM_YCnQ44K4SpeJqr-G1gWjQFgMwbAV1GbHRbuQJTLj5emVUXwg812ldDb6gzGzLNU5xnmvEkBmrPHtStLsEjenZtzMjUl2ZZ-AVMVahpQOPVFv9ArnXvs6tUpITJgA29daj7-YCy3NjhoBuVTfapQtqycr9qFKTr4TeiWwKGP4mpPOUjKHa8Kq_pZcWg8wTh5LssBi1AtSnbuc0lLLLphewTJQNB27hgM_BCoMQ69l10m201kDx7pH4D-q-v_2wSxfIvmUWqaUV3aqNLoItf-U1J94iwGRa4kldFc4sQ';
      $token = $request->input('access_token');
      $user = Socialite::driver('google')->userFromToken($token);
      $message = 'ok';
      $status = '1';
      return response()->json(['data' => $user,'message'=> $message,'status'=> $status]);
      //EXAMPLE
    }
    public function loginServer(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        // $request->merge(['email'=>'sa@tabsewa.com','password'=>'123456']);
        $result = $this->issueToken($request, 'password');
        $decode_token = json_decode($result->getContent());
        return $this->getLoginInfo($request,$decode_token->access_token);
    }
    public function getLoginInfo($request,$access_token)
    {
        $request->headers->set('Authorization', 'Bearer '.$access_token);
        $info = Route::dispatch(Request::create('api/profile/info', 'GET'));
        $decode_user = json_decode($info->getContent());
        $decode_user->access_token = $access_token;
        return response()->json($decode_user);
    }
    public function refresh(Request $request)
    {
        $this->validate($request, [
            'refresh_token' => 'required'
        ]);
        return $this->issueToken($request, 'refresh_token');
    }

    public function changePassword(Request $request)
    {
        $user = User::find($request->user()->id);
        $old_password = $request->input('old_password');
        $password = $request->input('password');
        $hash_password = $user->password;
        Validator::extend('password_hash', function ($attribute, $value, $parameters) {
            if ($value=="") {
                return true;
            }
            if (!Hash::check($value, $parameters[0])) {
                return false;
            }
            return true;
        });
        $messages = [
          'old_password.password_hash'    => 'The :attribute and last password must match.',
        ];
        $this->validate($request, [
            'old_password' => 'required|min:6|password_hash:' . $hash_password,
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ], $messages);

        $user->password=Hash::make($password);
        $user->save();
        $this->logout($request);
        $request->merge([
            'email' => $user->email,
        ]);
        return $this->login($request);
    }

    public function logout(Request $request)
    {
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')->where('access_token_id', $accessToken->id)->update(['revoked' => true]);
        $accessToken->revoke();
        return response()->json([], 204);
    }
}
