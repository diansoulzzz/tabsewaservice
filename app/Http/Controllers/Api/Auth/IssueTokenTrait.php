<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

trait IssueTokenTrait
{
    public function issueToken(Request $request, $grantType, $scope = "")
    {
        $params = [
            'grant_type' => $grantType,
            'client_id' => env('PASSWORD_CLIENT_ID'),
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
            'scope' => $scope
        ];
        if ($grantType !== 'social') {
            $params['username'] = $request->username ?: $request->email;
            $params['password'] = $request->password;
        }
        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        return Route::dispatch($proxy);
    }
    // public function issueToken2(Request $request, $grantType, $token= "", $scope = "")
    // {
    //     $params = [
    //         'grant_type' => $grantType,
    //         'scope' => $scope
    //     ];
    //     if ($grantType !== 'social') {
    //         $params['client_id'] = env('PASSWORD_CLIENT_ID');
    //         $params['client_secret'] = env('PASSWORD_CLIENT_SECRET');
    //         $params['username'] = $request->username ?: $request->email;
    //         $params['password'] = $request->password;
    //     } else {
    //         $params['client_id'] = env('PERSONAL_CLIENT_ID');
    //         $params['client_secret'] = env('PERSONAL_CLIENT_ID');
    //         $params['redirect_uri'] = 'http://localhost/tabsewa/callback';
    //         $params['code'] = $token;
    //     }
    //     $request->request->add($params);
    //     // return $request->all();
    //     $proxy = Request::create('oauth/token', 'POST');
    //     return Route::dispatch($proxy);
    // }
}
