<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
  public function index(Request $request)
  {
    return view('admin.menus.auth.login.content');
  }
  public function authenticate(Request $request)
  {
    $email = $request->input('email');
    $password = $request->input('password');
    if (Auth::attempt(['email' => $email, 'password' => $password])) {
      return redirect(route('admin.dashboard'));
    }
    return redirect(route('admin.login'))->withInput()->withErrors(['status' => 'username atau password salah mohon dicek kembali!']);
  }

  public function logout()
  {
    Auth::logout();
    return redirect(route('admin.login'));
  }

}
