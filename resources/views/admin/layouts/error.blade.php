<!doctype html>
<html lang="en">
<head>
  <title>@yield('title') - Tab Sewa</title>
  @include('admin.includes.meta')
  @yield('more-css','')
</head>

<body class="error_page app_theme_f">
  @yield('content')
</body>
</html>
