<aside id="sidebar_main">
  <div class="sidebar_main_header">
    <div class="sidebar_logo">
    </div>
  </div>
  <div class="menu_section">
    <ul>
      <li class="{{ Request::is('dashboard') ? 'current_section' : '' }}" title="Dashboard">
          <a href="{{ url('dashboard')}}">
              <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
              <span class="menu_title">Dashboard</span>
          </a>
      </li>
      <li>
          <a href="#">
            <span class="menu_icon"><i class="material-icons">&#xE060;</i></span>
            <span class="menu_title">Master</span>
          </a>
          <ul>
            <li>
                <a href="#"><span class="menu_title">Kategori</span></a>
                <ul>
                  <li class="{{ Request::is('master/kategori/input') ? 'act_item' : '' }}"><a href="{{ url('master/kategori/input') }}">Input</a></li>
                  <li class="{{ Request::is('master/kategori/list') ? 'act_item' : '' }}"><a href="{{ url('master/kategori/list') }}">List</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><span class="menu_title">SubKategori</span></a>
                <ul>
                  <li class="{{ Request::is('master/subkategori/input') ? 'act_item' : '' }}"><a href="{{ url('master/subkategori/input') }}">Input</a></li>
                  <li class="{{ Request::is('master/subkategori/list') ? 'act_item' : '' }}"><a href="{{ url('master/subkategori/list') }}">List</a></li>
                </ul>
            </li>
          </ul>
      </li>
    </ul>
  </div>
</aside>
