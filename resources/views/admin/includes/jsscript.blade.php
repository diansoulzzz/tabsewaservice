<script>
    WebFontConfig = {
        google: {
            families: [
                'Source+Code+Pro:400,700:latin',
                'Roboto:400,300,500,700,400italic:latin'
            ]
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
<script src="{{asset('assets/admin/js/common.min.js')}}"></script>
<script src="{{asset('assets/admin/js/uikit_custom.min.js')}}"></script>
<script src="{{asset('assets/admin/js/altair_admin_common.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
