@extends('admin.layouts.default')
@section('title', 'Dashboard')
@section('bread')
<div id="top_bar">
    <ul id="breadcrumbs">
        <li><a href="#">Home</a></li>
        <li><a href="#" style="color: grey">Dashboard</a></li>
    </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  <div class="uk-grid  uk-text-center uk-sortable sortable-handler uk-margin-bottom tabs-view-container" data-uk-sortable data-uk-grid-margin>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-light-blue-A400">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-users uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Customer</span>
                <h2 class="uk-margin-remove md-color-white"><span class="countUpMe">0<noscript>10</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-deep-orange-A700">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-info uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Outstanding Piutang</span>
                <h2 class="uk-margin-remove md-color-white"><span class="countUpMe">0<noscript>20</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-brown-700">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-check-square-o uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Total Piutang Belum Lunas</span>
                <h2 class="uk-margin-remove md-color-white">IDR <span class="countUpMe">0<noscript>30</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-yellow-900">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-dollar uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Total Laba Kotor</span>
                <h2 class="uk-margin-remove md-color-white">IDR <span class="countUpMe">0<noscript>40</noscript></span></h2>
            </div>
        </div>
    </div>
  </div>
</div>

@endsection
@push('b-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/admin/bower_components/sweetalert/dist/sweetalert.css')}}">
@endpush

@push('b-script')
<script>
    altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/admin/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/countUp.js/dist/countUp.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/d3/d3.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/c3js-chart/c3.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/highcharts/code/highcharts.js')}}"></script>
<script>
$(function() {
  currentform.count_animated(),
  currentform.circular_statistics();
}), currentform = {
  count_animated: function() {
    $(".countUpMe").each(function() {
      var e = this,
        t = $(e).text();
      theAnimation = new CountUp(e, 0, t, 0, 2), theAnimation.start()
    })
  },
  circular_statistics: function() {
    $(".epc_chart").easyPieChart({
      scaleColor: !1,
      trackColor: "#f5f5f5",
      lineWidth: 7,
      size: 150,
      easing: bez_easing_swiftOut
    })
  },
}
</script>
@endpush
