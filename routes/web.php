<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::domain('test.'.env('APP_URL_DOMAIN', 'localhost.com'))->group(function () {
//   Route::get('/', function () {
//       return "Connected";
//   });
// });
// Route::get('/', function () {
//     return "Connected";
// });

Route::group(['domain' => 'admin-tabsewa.'.env('APP_URL_DOMAIN', 'local.com'), 'middleware'=> ['web','revalidate']], function () {
  Route::get('/',function (){
    return redirect()->route('login');
  });
  Route::get('login', 'Admin\Auth\AdminLoginController@index')->name('login');
  Route::post('login','Admin\Auth\AdminLoginController@authenticate');

  Route::group(['middleware'=>'auth'],function(){
    Route::get('logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('dashboard', 'Admin\Dashboard\AdminDashboardController@index')->name('admin.dashboard');
    Route::group(['prefix'=>'master'],function (){
      Route::group(['prefix'=>'kategori'],function (){
        Route::get('input', 'Admin\Master\AdminMasterKategoriController@index')->name('admin.input.kategori');
        Route::post('input', 'Admin\Master\AdminMasterKategoriController@post');
        Route::get('list', 'Admin\Master\AdminMasterKategoriController@indexList');
        Route::options('list', 'Admin\Master\AdminMasterKategoriController@dataTable')->name('admin.list.kategori');
        Route::get('destroy', 'Admin\Master\AdminMasterKategoriController@delete')->name('admin.hapus.kategori');
      });
      Route::group(['prefix'=>'subkategori'],function (){
        Route::get('input', 'Admin\Master\AdminMasterSubKategoriController@index')->name('admin.input.subkategori');
        Route::post('input', 'Admin\Master\AdminMasterSubKategoriController@post');
        Route::get('list', 'Admin\Master\AdminMasterSubKategoriController@indexList');
        Route::options('list', 'Admin\Master\AdminMasterSubKategoriController@dataTable')->name('admin.list.subkategori');
        Route::get('destroy', 'Admin\Master\AdminMasterSubKategoriController@delete')->name('admin.hapus.subkategori');
      });
    });
  });
});
