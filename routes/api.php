<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('register', 'Api\Auth\RegisterController@register');
// Route::post('login', 'Api\Auth\LoginController@login');
// Route::post('refresh', 'Api\Auth\LoginController@refresh');

Route::prefix('login')->group(function () {
  Route::post('facebook', 'Api\Auth\LoginController@loginSocialFacebook');
  Route::post('google', 'Api\Auth\LoginController@loginSocialGoogle');
  Route::post('server', 'Api\Auth\LoginController@loginServer');
});
Route::prefix('home')->group(function () {
  Route::get('data', 'Api\Home\ApiHome@getData');
});
Route::prefix('item')->group(function () {
  Route::get('data', 'Api\Home\ApiHome@getItemDetail');
  Route::get('list', 'Api\Home\ApiHome@getItemList');
  Route::get('latest', 'Api\Home\ApiHome@getItemLatest');
});

Route::middleware('auth:api')->group(function () {
  Route::prefix('profile')->group(function () {
    Route::get('info', 'Api\ApiUser@getUserInfo');
  });

  // Route::get('user', 'Api\ApiUser@index');
});
