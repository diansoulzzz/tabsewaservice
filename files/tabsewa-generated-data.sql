CREATE DATABASE  IF NOT EXISTS `tabsewa` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tabsewa`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tabsewa
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `barang_sub_kategori_id` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_barang_sub_kategori1_idx` (`barang_sub_kategori_id`),
  KEY `fk_barang_users1_idx` (`users_id`),
  CONSTRAINT `fk_barang_barang_sub_kategori1` FOREIGN KEY (`barang_sub_kategori_id`) REFERENCES `barang_sub_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_barang_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES (1,'SAMSUNG',1,1,100,'2018-02-07 00:00:00','2018-02-07 00:00:00',NULL),(2,'TEST',1,1,12,'2018-02-07 00:00:00','2018-02-07 00:00:00','2018-02-07 00:00:00'),(3,'HP',1,1,100,'2018-02-07 00:00:00','2018-02-07 00:00:00',NULL),(4,'ASUS',1,1,11,'2018-02-07 00:00:00','2018-02-07 00:00:00',NULL),(5,'XIAOMI',1,1,55,'2018-02-07 00:00:00','2018-02-07 00:00:00',NULL);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_foto`
--

DROP TABLE IF EXISTS `barang_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_url` text,
  `barang_id` int(11) NOT NULL,
  `utama` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_foto_barang1_idx` (`barang_id`),
  CONSTRAINT `fk_barang_foto_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_foto`
--

LOCK TABLES `barang_foto` WRITE;
/*!40000 ALTER TABLE `barang_foto` DISABLE KEYS */;
INSERT INTO `barang_foto` VALUES (1,'https://s7d2.scene7.com/is/image/SamsungUS/Note8-Front-S-Pen-Midnight-Black?$product-details-jpg$',1,1),(2,'https://www.t-mobile.com/content/dam/t-mobile/en-p/cell-phones/samsung/samsung-galaxy-tabE/dark-gray/Samsung-Galaxy-TabE-Dark-Gray-1-3x.jpg',2,1);
/*!40000 ALTER TABLE `barang_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_harga_sewa`
--

DROP TABLE IF EXISTS `barang_harga_sewa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_harga_sewa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) NOT NULL,
  `lama_hari` int(11) DEFAULT NULL,
  `harga` decimal(14,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_harga_sewa_barang_barang1_idx` (`barang_id`),
  CONSTRAINT `fk_harga_sewa_barang_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_harga_sewa`
--

LOCK TABLES `barang_harga_sewa` WRITE;
/*!40000 ALTER TABLE `barang_harga_sewa` DISABLE KEYS */;
/*!40000 ALTER TABLE `barang_harga_sewa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_kategori`
--

DROP TABLE IF EXISTS `barang_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `foto_url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_kategori`
--

LOCK TABLES `barang_kategori` WRITE;
/*!40000 ALTER TABLE `barang_kategori` DISABLE KEYS */;
INSERT INTO `barang_kategori` VALUES (1,'ELEKTRONIK','https://cf.shopee.co.id/file/5bbb098620cf47305651831dad752a9c_tn','2018-02-07 00:00:00','2018-02-07 00:00:00',NULL),(2,'TEST',NULL,'2018-02-07 00:00:00','2018-02-07 00:00:00','2018-02-07 00:00:00');
/*!40000 ALTER TABLE `barang_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barang_sub_kategori`
--

DROP TABLE IF EXISTS `barang_sub_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_sub_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `barang_kategori_id` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  `foto_url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_barang_sub_kategori_barang_kategori1_idx` (`barang_kategori_id`),
  KEY `fk_barang_sub_kategori_users1_idx` (`users_id`),
  CONSTRAINT `fk_barang_sub_kategori_barang_kategori1` FOREIGN KEY (`barang_kategori_id`) REFERENCES `barang_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_barang_sub_kategori_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_sub_kategori`
--

LOCK TABLES `barang_sub_kategori` WRITE;
/*!40000 ALTER TABLE `barang_sub_kategori` DISABLE KEYS */;
INSERT INTO `barang_sub_kategori` VALUES (1,'TV',1,1,'http://abc.com','2018-02-07 00:00:00','2018-02-07 00:00:00',NULL),(2,'HP',1,1,'http://abc.com','2018-02-07 00:00:00','2018-02-07 00:00:00','2018-02-07 00:00:00');
/*!40000 ALTER TABLE `barang_sub_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  `tgl_awal` datetime DEFAULT NULL,
  `tgl_akhir` datetime DEFAULT NULL,
  `lama_sewa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cart_barang1_idx` (`barang_id`),
  KEY `fk_cart_users1_idx` (`users_id`),
  CONSTRAINT `fk_cart_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cart_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `d_sewa`
--

DROP TABLE IF EXISTS `d_sewa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d_sewa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) NOT NULL,
  `h_sewa_id` int(11) NOT NULL,
  `lama_sewa` int(11) DEFAULT NULL,
  `harga_sewa` decimal(14,2) DEFAULT NULL,
  `subtotal` decimal(14,2) DEFAULT NULL,
  `batas_pengembalian` datetime DEFAULT NULL,
  `tgl_kembali` datetime DEFAULT NULL,
  `approve_sewa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_d_sewa_h_sewa1_idx` (`h_sewa_id`),
  KEY `fk_d_sewa_cart1_idx` (`cart_id`),
  CONSTRAINT `fk_d_sewa_cart1` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_sewa_h_sewa1` FOREIGN KEY (`h_sewa_id`) REFERENCES `h_sewa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d_sewa`
--

LOCK TABLES `d_sewa` WRITE;
/*!40000 ALTER TABLE `d_sewa` DISABLE KEYS */;
/*!40000 ALTER TABLE `d_sewa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `h_piutang`
--

DROP TABLE IF EXISTS `h_piutang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `h_piutang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodenota` varchar(255) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `total_bayar` decimal(14,2) DEFAULT NULL,
  `payment_method` varchar(45) DEFAULT NULL,
  `users_peminjam_id` int(10) unsigned NOT NULL,
  `h_sewa_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_h_piutang_users1_idx` (`users_peminjam_id`),
  KEY `fk_h_piutang_h_sewa1_idx` (`h_sewa_id`),
  CONSTRAINT `fk_h_piutang_h_sewa1` FOREIGN KEY (`h_sewa_id`) REFERENCES `h_sewa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_h_piutang_users1` FOREIGN KEY (`users_peminjam_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `h_piutang`
--

LOCK TABLES `h_piutang` WRITE;
/*!40000 ALTER TABLE `h_piutang` DISABLE KEYS */;
/*!40000 ALTER TABLE `h_piutang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `h_sewa`
--

DROP TABLE IF EXISTS `h_sewa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `h_sewa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodenota` varchar(45) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `users_peminjam_id` int(10) unsigned NOT NULL,
  `grandtotal` decimal(14,2) DEFAULT NULL,
  `sisabayar` decimal(14,2) DEFAULT NULL,
  `terbayar` decimal(14,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_h_sewa_users1_idx` (`users_peminjam_id`),
  CONSTRAINT `fk_h_sewa_users1` FOREIGN KEY (`users_peminjam_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `h_sewa`
--

LOCK TABLES `h_sewa` WRITE;
/*!40000 ALTER TABLE `h_sewa` DISABLE KEYS */;
/*!40000 ALTER TABLE `h_sewa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('77d53522ccfcf0ea50aa83e225727168ff137cb67ca07d463bcbd4a59d9480f22bfa615fcce413ee',1,2,NULL,'[]',0,'2018-02-07 02:03:20','2018-02-07 02:03:20','2019-02-07 09:03:20'),('b0fc910c0d02a174ae1f50983feebd791f20789548d0edc299a7ddf5eb603a7f5ba84ed44f6ac29a',1,2,NULL,'[]',0,'2018-02-07 02:02:54','2018-02-07 02:02:54','2019-02-07 09:02:54');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','iNk0lADbg8sr7v8rh7jGjyolktOdHgyAUIlCXAsi','http://localhost',1,0,0,'2018-01-22 05:49:00','2018-01-22 05:49:00'),(2,NULL,'Laravel Password Grant Client','41BiFz3pOsuh7kXRN5qfdHj9bcpAXTsslTQNMOUE','http://localhost',0,1,0,'2018-01-22 05:49:00','2018-01-22 05:49:00');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2018-01-22 05:49:00','2018-01-22 05:49:00');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('179bd106a369ec8a1916c1464b6c675fc77eb9aee5bbe12ca5cf7d0d61497aefed7e55bb07e8e80b','b0fc910c0d02a174ae1f50983feebd791f20789548d0edc299a7ddf5eb603a7f5ba84ed44f6ac29a',0,'2019-02-07 09:02:54'),('d9cbdf261e5c8c654ac5e88a603a54b21f79c4455ad4b33f8e454e94b9d9f04aa7dc2008bd61031c','77d53522ccfcf0ea50aa83e225727168ff137cb67ca07d463bcbd4a59d9480f22bfa615fcce413ee',0,'2019-02-07 09:03:20');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_barang`
--

DROP TABLE IF EXISTS `review_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `barang_id` int(11) NOT NULL,
  `d_sewa_id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `komentar` text COLLATE utf8mb4_unicode_ci,
  `foto_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_has_barang_barang1_idx` (`barang_id`),
  KEY `fk_users_has_barang_users1_idx` (`users_id`),
  KEY `fk_review_barang_d_sewa1_idx` (`d_sewa_id`),
  CONSTRAINT `fk_review_barang_d_sewa1` FOREIGN KEY (`d_sewa_id`) REFERENCES `d_sewa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_barang_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_barang_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_barang`
--

LOCK TABLES `review_barang` WRITE;
/*!40000 ALTER TABLE `review_barang` DISABLE KEYS */;
/*!40000 ALTER TABLE `review_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_url` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `aktif` tinyint(4) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sa','sa@tabsewa.com','$2y$10$tMpQL9Nw.tcmOQAc/JZwnOf/B2mMN2LMrYCCBaU5q/LQPLfwBnbUm',NULL,NULL,'2018-02-05 02:43:39','2018-02-05 02:43:39',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09 17:32:26
